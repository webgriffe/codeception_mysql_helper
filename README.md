Codeception MySQL Helper
========================

This class is an helper for the Codeception testing framework ([http://codeception.com/](http://codeception.com/)).
Before every test, it restores provided database dump into your project's MySQL database.

Installation
------------

Install Codeception ([http://codeception.com/install](http://codeception.com/install)).  
Clone this repository somewhere on your development machine.

    $ git clone git@bitbucket.org:webgriffe/codeception_mysql_helper.git

Go to your tested project (in the root folder) and create a symbolic link to the helper.

    $ cd /path/to/your/project
    $ ln -s /path/to/codeception_mysql_helper/MysqlHelper.php tests/_helpers/MysqlHelper.php

With a symbolic link you can get updates simply by performing a git pull in the repository directory.

	$ cd /path/to/codeception_mysql_helper
	$ git pull

If you want, you can add to your _.gitignore_ MysqlHelper.php file because it is an external dependency. So put in your _.gitignore_ file the following:

	tests/_helpers/MysqlHelper.php
	

Configuration
-------------

Once installed the helper you have to configure it. In your _acceptance.suite.yml_ configuration file you can provide following parameters:

* host: MySQL hostname (default: 'localhost')
* user: MySQL username (default: 'root')
* password: MySQL password (default: no password)
* dbname: MySQL database name to restore
* dump: path to MySQL dump file (defualt: 'tests/_data/dump.sql')

For example, your your _acceptance.suite.yml_ configuration file could look like this:

	class_name: WebGuy
	modules:
    enabled: [PhpBrowser, MysqlHelper]
    config:
        PhpBrowser:
            url: 'http://myproject.local/'
        MysqlHelper:
            password: mypassword
            dbname: myproject