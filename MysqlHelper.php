<?php

namespace Codeception\Module;

/**
 * Helper for Codeception that restore provided MySQL database dump before every test.
 * See README.md file for further informations.
 *
 * @author Manuele Menozzi <mmenozzi@webgriffe.com>
 */
class MysqlHelper extends \Codeception\Module
{
    protected $config = array(
        'host' => 'localhost',
        'user' => 'root',
        'password' => '',
        'dump' => 'tests/_data/dump.sql',
    );

    protected $mySqlCommand;
    protected $dumpPath;

    public function _initialize()
    {
        if (empty($this->config['dbname'])) {
            throw new \Codeception\Exception\ModuleConfig(
                __CLASS__,
                "You must provide a MySQL database name.\n
                 Please, check your dbname config option."
            );
        }

        if (empty($this->config['password'])) {
            $this->mySqlCommand = sprintf(
                'mysql -h %s -u %s',
                $this->config['host'],
                $this->config['user']
            );
        }
        else {
            $this->mySqlCommand = sprintf(
                'mysql -h %s -u %s -p%s',
                $this->config['host'],
                $this->config['user'],
                $this->config['password']
            );
        }

        $this->dumpPath = getcwd() . DIRECTORY_SEPARATOR . $this->config['dump'];

        if (!file_exists($this->dumpPath)) {
            throw new \Codeception\Exception\ModuleConfig(
                __CLASS__,
                "File with dump deesn't exist.\n
                 Please, check path for sql file: " . $this->config['dump']
            );
        }
    }

    public function _before(\Codeception\TestCase $test)
    {
        if ($this->dropDatabase($this->config['dbname']) > 0) {
            throw new \RuntimeException('Database drop failed.');
        }

        if ($this->createDatabase($this->config['dbname']) > 0) {
            throw new \RuntimeException('Database creation failed.');
        }

        if ($this->restoreDatabase($this->config['dbname']) > 0) {
            throw new \RuntimeException('Database restore failed.');
        }
    }

    protected function dropDatabase($dbName)
    {
        if ($dbName == '') {
            throw new \InvalidArgumentException('Can\'t drop a no name database.');
        }

        $dropQuery = sprintf('DROP DATABASE IF EXISTS %s;', $dbName);

        $output = null;
        $outputArr = null;
        $dropCommand = sprintf('%s -e "%s"', $this->mySqlCommand, $dropQuery);
        exec($dropCommand, $outputArr, $output);

        return $output;
    }

    protected function createDatabase($dbName)
    {
        if ($dbName == '') {
            throw new \InvalidArgumentException('Can\'t create a no name database.');
        }

        $createQuery = sprintf('CREATE DATABASE %s;', $this->config['dbname']);

        $output = null;
        $outputArr = null;
        $createCommand = sprintf('%s -e "%s"', $this->mySqlCommand, $createQuery);
        exec($createCommand, $outputArr, $output);

        return $output;
    }

    protected function restoreDatabase($dbName)
    {
        if ($dbName == '') {
            throw new \InvalidArgumentException('Can\'t restore a no name database.');
        }

        $output = null;
        $outputArr = null;
        $restoreCommand = sprintf('%s %s < %s', $this->mySqlCommand, $dbName, $this->dumpPath);
        exec($restoreCommand, $outputArr, $output);

        return $output;
    }
}
